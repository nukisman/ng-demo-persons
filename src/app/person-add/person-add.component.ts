import { Component, OnInit } from '@angular/core';
import {PersonService} from '../../person.service';

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {
  newFirstName = '';
  newLastName = '';
  constructor(private personService: PersonService) { }

  ngOnInit() {
  }

  validate(): boolean {
    return this.newFirstName.length > 0 && this.newLastName.length > 0;
  }
  addPerson() {
    this.personService.addPerson(this.newFirstName, this.newLastName);
  }
}
