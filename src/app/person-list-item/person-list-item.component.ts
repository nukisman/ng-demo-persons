import {Component, Input, OnInit} from '@angular/core';
import {Person} from '../../person';
import {PersonService} from '../../person.service';

@Component({
  selector: 'app-person-list-item',
  templateUrl: './person-list-item.component.html',
  styleUrls: ['./person-list-item.component.css']
})
export class PersonListItemComponent implements OnInit {
  @Input()
  person: Person = null;
  constructor(private personService: PersonService) { }

  ngOnInit() {
  }

  deletePerson(): void {
    this.personService.deletePerson(this.person.id);
  }
}

