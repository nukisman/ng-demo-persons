import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonListComponent} from './person-list/person-list.component';
import {PersonEditComponent} from './person-edit/person-edit.component';

import {RandomGuard} from './random.guard';
import {NoAccessComponent} from './no-access/no-access.component';

const routes: Routes = [
  { path: '', redirectTo: '/persons', pathMatch: 'full' },
  { path: 'persons', component: PersonListComponent },
  { path: 'persons/:id', component: PersonEditComponent, canActivate: [RandomGuard] },
  { path: 'no-access', component: NoAccessComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
