import { Component, OnInit } from '@angular/core';
import {Person} from '../../person';
import {ActivatedRoute, Router} from '@angular/router';
import {PersonService} from '../../person.service';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {
  person: Person = null;
  newFirstName = '';
  newLastName = '';
  constructor(private route: ActivatedRoute, private personService: PersonService, private router: Router) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.person = this.personService.getById(id);
    this.newFirstName = this.person.firstName;
    this.newLastName = this.person.lastName;
  }

  validate(): boolean {
    return this.newFirstName.length > 0 && this.newLastName.length > 0;
  }

  updatePerson(): void {
    this.personService.updatePerson(this.person.id, this.newFirstName, this.newLastName);
    this.router.navigate([`/persons`]);
  }
}
