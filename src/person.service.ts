import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Person } from './person';

const key = 'persons';

@Injectable({ providedIn: 'root' })
export class PersonService {
  persons = [];
  constructor() {
    this.persons = JSON.parse(localStorage.getItem(key)) || [];
  }
  save() {
    localStorage.setItem(key, JSON.stringify(this.persons));
  }

  getNextId(): number {
    return 1 + Math.max(-1, ...this.persons.map(p => p.id));
  }

  getPersons(): Observable<Person[]> {
    return of(this.persons);
  }
  addPerson(firstName: string, lastName: string): void {
    this.persons.push(new Person(this.getNextId(), firstName, lastName));
    this.save();
  }
  getById(id: number): Person {
    return this.persons.find(p => p.id === id);
  }
  updatePerson(id: number, firstName: string, lastName: string): void {
    const found = this.persons.find(p => p.id === id);
    if (found) {
      found.firstName = firstName;
      found.lastName = lastName;
    }
    this.save();
  }
  deletePerson(id: number): void {
    const index = this.persons.findIndex(p => p.id === id);
    this.persons.splice(index, 1);
    this.save();
  }
}
